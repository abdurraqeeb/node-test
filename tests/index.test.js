import { describe, it } from 'node:test';
import assert from 'node:assert';
import { service, addService } from '../index.js';

describe('Index', () => {
  describe('Increase By Twelve', () => {
    it('should add twelve', (t) => {
      t.mock.fn(addService, 'ten').mock.mockImplementation(() => {
        return 3;
      });

      const result = service.twelve(1);

      assert.equal(result, 13);
    });
  });
});
