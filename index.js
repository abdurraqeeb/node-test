export function add() {
  function one(x) {
    return x + 1;
  }

  function ten(y) {
    return y + 10;
  }

  return { one, ten };
}

export function increaseService(fn) {
  function two(x) {
    return x + fn.one(x);
  }

  function twelve(x) {
    return 2 + fn.ten(x);
  }

  return { two, twelve };
}

export const addService = add();

export const service = increaseService(addService);
